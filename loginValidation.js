const product = document.getElementsByClassName('container');

let logincredential = {'vaibhavniwale':'vaibhavniwale'}

document.querySelector('.loginclick').addEventListener('click',(event)=>{ 
    document.querySelector('.logintab').classList.remove('hidden')
    document.querySelector('.signuptab').classList.add('hidden')
    document.querySelector('.forgottab').classList.add('hidden')

    document.querySelector('.loginclick').classList.remove('hover')
    document.querySelector('.loginclick').classList.add('selected')
    document.querySelector('.signupclick').classList.remove('selected')
    document.querySelector('.signupclick').classList.add('hover')
    document.querySelector('.forgetclick').classList.remove('selected')
    document.querySelector('.forgetclick').classList.add('hover')
})

document.querySelector('.signupclick').addEventListener('click',(event)=>{ 
    document.querySelector('.logintab').classList.add('hidden')
    document.querySelector('.signuptab').classList.remove('hidden')
    document.querySelector('.forgottab').classList.add('hidden')

    document.querySelector('.loginclick').classList.remove('selected')
    document.querySelector('.loginclick').classList.add('hover')
    document.querySelector('.signupclick').classList.remove('hover')
    document.querySelector('.signupclick').classList.add('selected')
    document.querySelector('.forgetclick').classList.remove('selected')
    document.querySelector('.forgetclick').classList.add('hover')
})

document.querySelector('.forgetclick').addEventListener('click',(event)=>{ 
    document.querySelector('.logintab').classList.add('hidden')
    document.querySelector('.signuptab').classList.add('hidden')
    document.querySelector('.forgottab').classList.remove('hidden')

    document.querySelector('.loginclick').classList.remove('selected')
    document.querySelector('.loginclick').classList.add('hover')
    document.querySelector('.signupclick').classList.remove('selected')
    document.querySelector('.signupclick').classList.add('hover')
    document.querySelector('.forgetclick').classList.remove('hover')
    document.querySelector('.forgetclick').classList.add('selected')
})

const username = document.querySelector('.username')
username.addEventListener('input',(event)=>{
    if(event.target.value.length < 8){
        document.querySelector('.instructions').innerText = 'must be 8 characters'
    }else{
        document.querySelector('.instructions').innerText = ''
    }
})

const password = document.querySelector('.password')
password.addEventListener('input',(event)=>{
    if(event.target.value.length < 8){
        document.querySelector(".passinsructions").innerText = 'must be 8 characters'
    }else{
        document.querySelector(".passinsructions").innerText = ''
    }
})

const submit = document.querySelector('.submit')
submit.addEventListener('click',(event)=>{
    if(username.value.length > 7 && password.value.length > 7){
        if (logincredential[username.value] === password.value){
            document.getElementsByClassName("login").innerText = 'login successfully'    
        }else{
            document.getElementsByClassName("login").innerText = 'wrong username or password'
        }
    }else{
        document.getElementsByClassName("login").innerText = 'must be 8 characters'
    } 
})

document.querySelector('.transform').addEventListener('click',(event)=>{
    if(document.querySelector('body').classList.contains('dark')){
        document.querySelector('body').classList.remove('dark')
        document.querySelector('body').classList.add('light')
        document.querySelector('.image').classList.add('hidden')
        document.querySelector('.image2').classList.remove('hidden')
        document.querySelector('.transform').classList.add('headimg2')
        document.querySelector('.transform').classList.remove('headimg')
    } else{
    document.querySelector('body').classList.add('dark')
    document.querySelector('body').classList.remove('light')
    document.querySelector('.image').classList.remove('hidden')
    document.querySelector('.image2').classList.add('hidden')
    document.querySelector('.transform').classList.remove('headimg2')
    document.querySelector('.transform').classList.add('headimg')
    }
    
})
